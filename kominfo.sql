-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 26, 2019 at 09:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kominfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pendapatan`
--

CREATE TABLE `tbl_pendapatan` (
  `idkeuangan_pendpt_daerah` int(11) NOT NULL,
  `jenis_pendapatan` varchar(255) NOT NULL,
  `nominal` bigint(20) NOT NULL,
  `tahun` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pendapatan`
--

INSERT INTO `tbl_pendapatan` (`idkeuangan_pendpt_daerah`, `jenis_pendapatan`, `nominal`, `tahun`, `type`) VALUES
(1, 'Belanja Tidak Langsung', 1000000, 2017, 'Belanja Pegawai'),
(2, 'Belanja Tidak Langsung', 1500000, 2018, 'Belanja Pegawai'),
(3, 'Belanja Tidak Langsung', 234000, 2017, 'Belanja Subsidi'),
(4, 'Belanja Tidak Langsung', 723000, 2018, 'Belanja Subsidi'),
(5, 'Belanja Langsung', 1500000, 2017, 'Belanja Barang'),
(6, 'Belanja Langsung', 2000000, 2018, 'Belanja Jasa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pendapatan`
--
ALTER TABLE `tbl_pendapatan`
  ADD PRIMARY KEY (`idkeuangan_pendpt_daerah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pendapatan`
--
ALTER TABLE `tbl_pendapatan`
  MODIFY `idkeuangan_pendpt_daerah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
