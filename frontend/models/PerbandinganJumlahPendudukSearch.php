<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\PerbandinganJumlahPenduduk;

/**
 * PerbandinganJumlahPendudukSearch represents the model behind the search form of `frontend\models\PerbandinganJumlahPenduduk`.
 */
class PerbandinganJumlahPendudukSearch extends PerbandinganJumlahPenduduk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idperbandingan_jumlah_penduduk', 'tahun'], 'integer'],
            [['kabupaten_kota', 'type'], 'safe'],
            [['jmlh_penduduk'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerbandinganJumlahPenduduk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idperbandingan_jumlah_penduduk' => $this->idperbandingan_jumlah_penduduk,
            'jmlh_penduduk' => $this->jmlh_penduduk,
            'tahun' => $this->tahun,
        ]);

        $query->andFilterWhere(['like', 'kabupaten_kota', $this->kabupaten_kota])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }

    public function getJumlahPendudukCompare() {
        $tahun = PerbandinganJumlahPenduduk::find()->select(['tahun'])->orderBy(['tahun'=> SORT_DESC])->asArray()->one();
        $tahuns = [];
        for($i=4;$i>=0;$i--) {
            $tahuns[] = strval($tahun['tahun'] - $i);
        }
        
        $connection = \Yii::$app->db;
        $command = $connection->createCommand("
          SELECT a.idperbandingan_jumlah_penduduk, a.kabupaten_kota, a.type,
          SUM(IF(tahun=:tahun1, a.jmlh_penduduk, 0)) as :tahun1,
          SUM(IF(tahun=:tahun2, a.jmlh_penduduk, 0)) as :tahun2,
          SUM(IF(tahun=:tahun3, a.jmlh_penduduk, 0)) as :tahun3,
          SUM(IF(tahun=:tahun4, a.jmlh_penduduk, 0)) as :tahun4,
          SUM(IF(tahun=:tahun5, a.jmlh_penduduk, 0)) as :tahun5
           FROM `tbl_perbandingan_jumlah_penduduk` a GROUP BY kabupaten_kota ORDER BY tahun
          ",[':tahun1'=>$tahuns[0],':tahun2'=>$tahuns[1],':tahun3'=>$tahuns[2],':tahun4'=>$tahuns[3],':tahun5'=>$tahuns[4]]);
        $model = $command->queryAll();

        return [
            'model'=>$model,
            'tahuns'=>$tahuns
        ];
    }
}
