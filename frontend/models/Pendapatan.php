<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%pendapatan}}".
 *
 * @property int $idkeuangan_pendpt_daerah
 * @property string $jenis_pendapatan
 * @property string $nominal
 * @property int $tahun
 * @property string $type
 */
class Pendapatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pendapatan}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_pendapatan', 'nominal', 'tahun', 'type'], 'required'],
            [['nominal', 'tahun'], 'integer'],
            [['jenis_pendapatan', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idkeuangan_pendpt_daerah' => Yii::t('app', 'Idkeuangan Pendpt Daerah'),
            'jenis_pendapatan' => Yii::t('app', 'Jenis Pendapatan'),
            'nominal' => Yii::t('app', 'Nominal'),
            'tahun' => Yii::t('app', 'Tahun'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
