<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Pendapatan;

/**
 * PendapatanSearch represents the model behind the search form of `frontend\models\Pendapatan`.
 */
class PendapatanSearch extends Pendapatan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idkeuangan_pendpt_daerah', 'nominal', 'tahun'], 'integer'],
            [['jenis_pendapatan', 'type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pendapatan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idkeuangan_pendpt_daerah' => $this->idkeuangan_pendpt_daerah,
            'nominal' => $this->nominal,
            'tahun' => $this->tahun,
        ]);

        $query->andFilterWhere(['like', 'jenis_pendapatan', $this->jenis_pendapatan])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }

    public function getPendapatanCompare() {
        $tahun = Pendapatan::find()->select(['tahun'])->orderBy(['tahun'=> SORT_DESC])->asArray()->one();
        $tahuns = [strval($tahun["tahun"]-1),$tahun["tahun"]];
        
        $connection = \Yii::$app->db;
        $command = $connection->createCommand("
           select a.jenis_pendapatan, a.type, b.nominal as '2017', c.nominal as '2018' 
           from (select jenis_pendapatan,type, tahun from tbl_pendapatan group by type order by jenis_pendapatan, tahun, type) as a 
            LEFT OUTER JOIN (select jenis_pendapatan, type, tahun, nominal from tbl_pendapatan where tahun=:tahun1 order by jenis_pendapatan, type) as b on a.type=b.type
            LEFT OUTER JOIN (select jenis_pendapatan, type, tahun, nominal from tbl_pendapatan where tahun=:tahun2 order by jenis_pendapatan, type) as c on a.type=c.type",[':tahun1'=>$tahuns[0],':tahun2'=>$tahuns[1]]);
        $model = $command->queryAll();

        return [
            'model'=>$model,
            'tahuns'=>$tahuns
        ];
    }
}
