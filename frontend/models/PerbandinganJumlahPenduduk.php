<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%perbandingan_jumlah_penduduk}}".
 *
 * @property int $idperbandingan_jumlah_penduduk
 * @property string $kabupaten_kota
 * @property string $type
 * @property double $jmlh_penduduk
 * @property int $tahun
 */
class PerbandinganJumlahPenduduk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%perbandingan_jumlah_penduduk}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kabupaten_kota', 'type', 'jmlh_penduduk', 'tahun'], 'required'],
            [['jmlh_penduduk'], 'number'],
            [['tahun'], 'integer'],
            [['kabupaten_kota', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idperbandingan_jumlah_penduduk' => Yii::t('app', 'Idperbandingan Jumlah Penduduk'),
            'kabupaten_kota' => Yii::t('app', 'Kabupaten Kota'),
            'type' => Yii::t('app', 'Type'),
            'jmlh_penduduk' => Yii::t('app', 'Jmlh Penduduk'),
            'tahun' => Yii::t('app', 'Tahun'),
        ];
    }
}
