<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\PerbandinganJumlahPenduduk */

$this->title = $model->idperbandingan_jumlah_penduduk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perbandingan Jumlah Penduduks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="perbandingan-jumlah-penduduk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idperbandingan_jumlah_penduduk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idperbandingan_jumlah_penduduk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idperbandingan_jumlah_penduduk',
            'kabupaten_kota',
            'type',
            'jmlh_penduduk',
            'tahun',
        ],
    ]) ?>

</div>
