<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PerbandinganJumlahPenduduk */

$this->title = Yii::t('app', 'Update Perbandingan Jumlah Penduduk: {name}', [
    'name' => $model->idperbandingan_jumlah_penduduk,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perbandingan Jumlah Penduduks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idperbandingan_jumlah_penduduk, 'url' => ['view', 'id' => $model->idperbandingan_jumlah_penduduk]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="perbandingan-jumlah-penduduk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
