<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PerbandinganJumlahPenduduk */

$this->title = Yii::t('app', 'Create Perbandingan Jumlah Penduduk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perbandingan Jumlah Penduduks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perbandingan-jumlah-penduduk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
