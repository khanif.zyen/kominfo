<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PerbandinganJumlahPendudukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Perbandingan Jumlah Penduduks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perbandingan-jumlah-penduduk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Perbandingan Jumlah Penduduk'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idperbandingan_jumlah_penduduk',
            'kabupaten_kota',
            'type',
            'jmlh_penduduk',
            'tahun',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
