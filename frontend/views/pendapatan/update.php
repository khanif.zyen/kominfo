<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pendapatan */

$this->title = Yii::t('app', 'Update Pendapatan: {name}', [
    'name' => $model->idkeuangan_pendpt_daerah,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pendapatans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idkeuangan_pendpt_daerah, 'url' => ['view', 'id' => $model->idkeuangan_pendpt_daerah]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pendapatan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
