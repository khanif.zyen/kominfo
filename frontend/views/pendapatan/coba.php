<table border="1">
<tr><td><b>Jenis belanja</b></td>
	<?php 
	//looping tahun terakhir yang didapat
	foreach ($tahuns as $tahun){
		echo "<td><b>$tahun</b></td>";
	}
	?>
</tr>

<?php

$jenis_pendapatan ='';
$jenis_pendapatan_count = 1;
$total1=0;
$total2=0;

foreach ($model as $data){
	if($jenis_pendapatan!='' && ($data['jenis_pendapatan']!=$jenis_pendapatan)) //untuk jenis pendapatan lebih dari satu, maka total akan ditampilkan di looping awal. untuk jenis pendapatan cuma 1, maka dicetak diluar loop. 
		echo "<tr><td><b>Total ".$jenis_pendapatan."</b></td><td><b>".number_format($total1)."</b></td><td><b>".number_format($total2). "</b></td></tr>";
	echo "<tr>";
	if ($data['jenis_pendapatan']!=$jenis_pendapatan){
		echo "<td colspan='3'>".$jenis_pendapatan_count.'. '.$data['jenis_pendapatan']."</td></tr>";
		$jenis_pendapatan = $data['jenis_pendapatan'];
		$jenis_pendapatan_count++;
		$type_count=1;
		$total1=0;
		$total2=0;
	}
	echo "<td>".($jenis_pendapatan_count-1).".".$type_count.' '.$data['type']."</td><td align='right'>".number_format($data[(int)$tahuns[0]])."</td><td align='right'>".number_format($data[(int)$tahuns[1]])."</td></tr>";
	$total1=$total1 + $data[(int)$tahuns[0]];
	$total2=$total2 + $data[(int)$tahuns[1]];
	$type_count++;
}

echo "<tr><td><b>Total ".$data['jenis_pendapatan']."</b></td><td align='right'><b>".number_format($total1)."</b></td><td align='right'><b>".number_format($total2). "</b></td></tr>";

?>
<tr></tr>
</table>